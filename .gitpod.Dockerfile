FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update && \
    sudo apt-get install -y libsecret-1-dev && \
    sudo rm -rf /var/lib/apt/lists/*
